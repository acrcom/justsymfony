#!/bin/bash
set -e
# Variables
PROJECT_DIR="/var/www/JustSymfony/"
BUILDS_DIR="${PROJECT_DIR}/builds/"
STAGE_DIR="${PROJECT_DIR}/staging/"
PROD_DIR="${PROJECT_DIR}/production/"
BUILD_NUMBER=${BITBUCKET_BUILD_NUMBER}
BUILD_NAME="${BUILD_NUMBER}"
ENVIRONMENT="${BITBUCKET_DEPLOYMENT_ENVIRONMENT}"

echo 1
if [[ ${ENVIRONMENT} == "staging" ]]; then
    # Deploy to staging
    mkdir "${BUILDS_DIR}/${BUILD_NAME}"
    chown :www-data "${BUILDS_DIR}/${BUILD_NAME}"
    cd "${BUILDS_DIR}/${BUILD_NAME}"
    git clone git@bitbucket.org:acrcom/justsymfony.git .
    composer install
    echo "Build are complete" > .ok
    cd "${STAGE_DIR}"
    cp -rp "${BUILDS_DIR}/${BUILD_NAME}" "${BUILD_NAME}"
    ln -sfn "${BUILD_NAME}" CURRENT
elif [[ ${ENVIRONMENT} == "production" ]]; then
    # Deploy to production
    cd "${PROD_DIR}"
    cp -rp "${BUILDS_DIR}/${BUILD_NAME}" "${BUILD_NAME}"
    ln -sfn "${BUILD_NAME}" CURRENT
else
    echo "I don't know what to do with \"${ENVIRONMENT}\" environment"
fi